/*
Navicat MySQL Data Transfer

Source Server         : test
Source Server Version : 50732
Source Host           : 192.168.56.10:3306
Source Database       : tsy_pms

Target Server Type    : MYSQL
Target Server Version : 50732
File Encoding         : 65001

Date: 2020-12-21 04:09:20
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `mArea`
-- ----------------------------
DROP TABLE IF EXISTS `mArea`;
CREATE TABLE `mArea` (
  `mArea_id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '监测域编号',
  `mArea_alert_level` int(10) unsigned NOT NULL COMMENT '告警级别',
  `mArea_begin` bigint(20) NOT NULL COMMENT '起始里程',
  `mArea_end` bigint(20) NOT NULL COMMENT '结束里程',
  PRIMARY KEY (`mArea_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COMMENT='监测域表';

-- ----------------------------
-- Records of mArea
-- ----------------------------
INSERT INTO `mArea` VALUES ('00000000000000000001', '1', '210', '1068');
INSERT INTO `mArea` VALUES ('00000000000000000002', '2', '279', '493');
INSERT INTO `mArea` VALUES ('00000000000000000003', '3', '1925', '2580');
INSERT INTO `mArea` VALUES ('00000000000000000004', '4', '1067', '1494');
INSERT INTO `mArea` VALUES ('00000000000000000005', '1', '832', '1716');
INSERT INTO `mArea` VALUES ('00000000000000000006', '3', '120', '1899');
INSERT INTO `mArea` VALUES ('00000000000000000007', '2', '741', '2573');
INSERT INTO `mArea` VALUES ('00000000000000000008', '3', '1323', '1601');
INSERT INTO `mArea` VALUES ('00000000000000000009', '4', '616', '2540');
INSERT INTO `mArea` VALUES ('00000000000000000010', '3', '543', '1751');
INSERT INTO `mArea` VALUES ('00000000000000000011', '2', '421', '764');
INSERT INTO `mArea` VALUES ('00000000000000000012', '3', '452', '827');
INSERT INTO `mArea` VALUES ('00000000000000000013', '2', '296', '1226');
INSERT INTO `mArea` VALUES ('00000000000000000014', '1', '882', '2378');
INSERT INTO `mArea` VALUES ('00000000000000000015', '3', '1191', '1329');
INSERT INTO `mArea` VALUES ('00000000000000000016', '2', '1508', '2184');
INSERT INTO `mArea` VALUES ('00000000000000000017', '4', '557', '599');
INSERT INTO `mArea` VALUES ('00000000000000000018', '4', '1899', '3629');
INSERT INTO `mArea` VALUES ('00000000000000000019', '4', '328', '736');
INSERT INTO `mArea` VALUES ('00000000000000000020', '2', '423', '721');
INSERT INTO `mArea` VALUES ('00000000000000000021', '3', '640', '2420');
INSERT INTO `mArea` VALUES ('00000000000000000022', '2', '1219', '1865');
INSERT INTO `mArea` VALUES ('00000000000000000023', '1', '431', '880');
INSERT INTO `mArea` VALUES ('00000000000000000024', '1', '275', '686');
INSERT INTO `mArea` VALUES ('00000000000000000025', '1', '1401', '2475');
INSERT INTO `mArea` VALUES ('00000000000000000026', '2', '1245', '1477');
INSERT INTO `mArea` VALUES ('00000000000000000027', '3', '1695', '3219');
INSERT INTO `mArea` VALUES ('00000000000000000028', '4', '1087', '1134');
INSERT INTO `mArea` VALUES ('00000000000000000029', '4', '1986', '3732');
INSERT INTO `mArea` VALUES ('00000000000000000030', '4', '221', '445');
INSERT INTO `mArea` VALUES ('00000000000000000031', '1', '1209', '1282');
INSERT INTO `mArea` VALUES ('00000000000000000032', '4', '140', '1526');
INSERT INTO `mArea` VALUES ('00000000000000000033', '3', '426', '1388');
INSERT INTO `mArea` VALUES ('00000000000000000034', '1', '1704', '2771');
INSERT INTO `mArea` VALUES ('00000000000000000035', '4', '1825', '2289');
INSERT INTO `mArea` VALUES ('00000000000000000036', '2', '1412', '1821');
INSERT INTO `mArea` VALUES ('00000000000000000037', '4', '1300', '1703');
INSERT INTO `mArea` VALUES ('00000000000000000038', '2', '1398', '2455');
INSERT INTO `mArea` VALUES ('00000000000000000039', '3', '731', '989');
INSERT INTO `mArea` VALUES ('00000000000000000040', '1', '1584', '1801');

-- ----------------------------
-- Table structure for `mBody`
-- ----------------------------
DROP TABLE IF EXISTS `mBody`;
CREATE TABLE `mBody` (
  `mBody_id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '检测体编号',
  `mBody_name` varchar(50) NOT NULL COMMENT '监测体名称',
  `mBody_type` varchar(20) NOT NULL COMMENT '监测体类型',
  `mBody_status` varchar(20) NOT NULL COMMENT '监测体状态',
  `mBody_is_section` bit(1) NOT NULL COMMENT '是否区段',
  `mBody_begin` bigint(20) NOT NULL COMMENT '起始里程',
  `mBody_end` bigint(20) DEFAULT NULL COMMENT '结束里程',
  PRIMARY KEY (`mBody_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4 COMMENT='监测体表';

-- ----------------------------
-- Records of mBody
-- ----------------------------
INSERT INTO `mBody` VALUES ('00000000000000000001', '监测体cf825d9f-5030-447e-ba20-b48f0d22a7f9', '监测体类型2', '进行中', '', '1148', '2513');
INSERT INTO `mBody` VALUES ('00000000000000000002', '监测体30e62d8d-37f6-40bc-80e6-cde613eac64d', '监测体类型4', '进行中', '', '894', '942');
INSERT INTO `mBody` VALUES ('00000000000000000003', '监测体ef94fa8d-1ff1-4646-84e3-e82010c5e277', '监测体类型1', '进行中', '', '722', '997');
INSERT INTO `mBody` VALUES ('00000000000000000004', '监测体a69308e3-dd99-47e7-ae56-92c1f94a74bb', '监测体类型2', '进行中', '', '1461', '2108');
INSERT INTO `mBody` VALUES ('00000000000000000005', '监测体c3478040-142c-4d4f-80b3-6dcd430fa44b', '监测体类型4', '进行中', '', '93', '1280');
INSERT INTO `mBody` VALUES ('00000000000000000006', '监测体f3ba1370-0cf0-440b-90b4-66f95089a6e3', '监测体类型2', '进行中', '', '1681', '2590');
INSERT INTO `mBody` VALUES ('00000000000000000007', '监测体a1048407-1377-4615-b2a1-445850cf497b', '监测体类型4', '进行中', '', '903', '1636');
INSERT INTO `mBody` VALUES ('00000000000000000008', '监测体41cdcdd2-a70e-499a-a3cc-2b9e1f34acdf', '监测体类型4', '进行中', '', '1672', '2857');
INSERT INTO `mBody` VALUES ('00000000000000000009', '监测体ab7b3a55-6428-449f-b42d-a39fd37adcd7', '监测体类型2', '进行中', '', '848', '1415');
INSERT INTO `mBody` VALUES ('00000000000000000010', '监测体6be98adc-4a95-4f20-9bbb-3c6ef0686fed', '监测体类型3', '进行中', '', '1746', '2860');
INSERT INTO `mBody` VALUES ('00000000000000000011', '监测体7e9da6a9-6ab3-42de-8c91-cda2d16bf43c', '监测体类型1', '进行中', '', '1560', '2243');
INSERT INTO `mBody` VALUES ('00000000000000000012', '监测体c2d84732-25a0-4f22-9be2-453726f850dd', '监测体类型4', '进行中', '', '1464', '3267');
INSERT INTO `mBody` VALUES ('00000000000000000013', '监测体013ea936-d7e0-4f6c-b76a-78111a192fb8', '监测体类型2', '进行中', '', '1507', '1558');
INSERT INTO `mBody` VALUES ('00000000000000000014', '监测体fea39a3c-f2c3-4662-ae23-42c2773ff31b', '监测体类型0', '进行中', '', '487', '2408');
INSERT INTO `mBody` VALUES ('00000000000000000015', '监测体ca721399-2c4a-481d-a847-292c53c3f009', '监测体类型2', '进行中', '', '420', '1139');
INSERT INTO `mBody` VALUES ('00000000000000000016', '监测体409721f8-dbc5-4f77-a9c6-ad7d26d6f670', '监测体类型4', '进行中', '', '1147', '1996');
INSERT INTO `mBody` VALUES ('00000000000000000017', '监测体939a310c-b289-4e10-ac82-cf8af0641484', '监测体类型1', '进行中', '', '385', '1516');
INSERT INTO `mBody` VALUES ('00000000000000000018', '监测体85d81fec-b473-4afd-8e44-896861d9610a', '监测体类型2', '进行中', '', '516', '577');
INSERT INTO `mBody` VALUES ('00000000000000000019', '监测体f4bbc84d-28ee-4a9c-ac16-1db42f474163', '监测体类型3', '进行中', '', '1941', '2369');
INSERT INTO `mBody` VALUES ('00000000000000000020', '监测体c131d469-de0b-4db4-a980-c899c621f7ed', '监测体类型4', '进行中', '', '99', '641');
INSERT INTO `mBody` VALUES ('00000000000000000021', '监测体37c5365c-0933-489a-87e4-6e3c8415ebb5', '监测体类型0', '进行中', '', '136', '1097');
INSERT INTO `mBody` VALUES ('00000000000000000022', '监测体966780d5-696e-4b37-b688-55f364a5971b', '监测体类型3', '进行中', '', '1869', '3813');
INSERT INTO `mBody` VALUES ('00000000000000000023', '监测体f0aee5e1-2955-435b-ae89-0755c85094c2', '监测体类型2', '进行中', '', '1849', '2908');
INSERT INTO `mBody` VALUES ('00000000000000000024', '监测体a463ed0b-eacc-42e3-a139-631d840888e9', '监测体类型4', '进行中', '', '1382', '3008');
INSERT INTO `mBody` VALUES ('00000000000000000025', '监测体7746f51f-f01d-42a0-b68f-0c4c830cac57', '监测体类型3', '进行中', '', '132', '1643');
INSERT INTO `mBody` VALUES ('00000000000000000026', '监测体3ce9620f-64c5-4259-b738-43c93b63cd84', '监测体类型0', '进行中', '', '1815', '3316');
INSERT INTO `mBody` VALUES ('00000000000000000027', '监测体dbfdf770-cd8f-4264-ba81-2a2a1a9b2186', '监测体类型1', '进行中', '', '332', '836');
INSERT INTO `mBody` VALUES ('00000000000000000028', '监测体257fd9ea-1d6a-46dd-89b7-98305b920e2a', '监测体类型2', '进行中', '', '889', '2075');
INSERT INTO `mBody` VALUES ('00000000000000000029', '监测体dfc5b1c3-fb93-4ca0-9a63-3a30539cbb6f', '监测体类型4', '进行中', '', '1202', '2353');
INSERT INTO `mBody` VALUES ('00000000000000000030', '监测体09c776a4-68fd-417e-b7f7-8ba53fcfcf7d', '监测体类型2', '进行中', '', '1079', '2573');
INSERT INTO `mBody` VALUES ('00000000000000000031', '监测体785f4486-6f92-4ff0-be7c-50fecba05b44', '监测体类型0', '进行中', '', '538', '1495');
INSERT INTO `mBody` VALUES ('00000000000000000032', '监测体e471e79f-8ba4-4043-88d1-2442de7ee8a6', '监测体类型1', '进行中', '', '623', '623');
INSERT INTO `mBody` VALUES ('00000000000000000033', '监测体52294eca-ca2d-4621-ad8b-1b64e4c9e529', '监测体类型3', '进行中', '', '77', '1226');
INSERT INTO `mBody` VALUES ('00000000000000000034', '监测体9e029ceb-9059-482c-936d-2feb7e77e1c1', '监测体类型1', '进行中', '', '1841', '2432');
INSERT INTO `mBody` VALUES ('00000000000000000035', '监测体1bb11752-8744-4de2-99e1-55af5423bf4a', '监测体类型4', '进行中', '', '1505', '2277');
INSERT INTO `mBody` VALUES ('00000000000000000036', '监测体df29e83c-017a-45c8-9354-2a18c45fd573', '监测体类型2', '进行中', '', '206', '651');
INSERT INTO `mBody` VALUES ('00000000000000000037', '监测体e2b11a5e-2263-42e3-8c46-a98ac55ff43a', '监测体类型0', '进行中', '', '850', '2597');
INSERT INTO `mBody` VALUES ('00000000000000000038', '监测体2e39dae1-1084-42ba-bd9b-eadc016d2583', '监测体类型4', '进行中', '', '745', '2009');
INSERT INTO `mBody` VALUES ('00000000000000000039', '监测体3287ec6d-a3f8-440e-8868-485ca6249e75', '监测体类型1', '进行中', '', '413', '2046');
INSERT INTO `mBody` VALUES ('00000000000000000040', '监测体9f079697-c212-4e04-9177-707541d535f3', '监测体类型0', '进行中', '', '473', '2463');

-- ----------------------------
-- Table structure for `mBody_type`
-- ----------------------------
DROP TABLE IF EXISTS `mBody_type`;
CREATE TABLE `mBody_type` (
  `mBody_type_id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '监测体类型编号',
  `mBody_type` varchar(50) NOT NULL COMMENT '监测体类型名称',
  PRIMARY KEY (`mBody_type_id`),
  UNIQUE KEY `name` (`mBody_type`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='监测体类型表';

-- ----------------------------
-- Records of mBody_type
-- ----------------------------
INSERT INTO `mBody_type` VALUES ('00000000000000000001', '监测体类型0');
INSERT INTO `mBody_type` VALUES ('00000000000000000002', '监测体类型1');
INSERT INTO `mBody_type` VALUES ('00000000000000000003', '监测体类型2');
INSERT INTO `mBody_type` VALUES ('00000000000000000004', '监测体类型3');
INSERT INTO `mBody_type` VALUES ('00000000000000000005', '监测体类型4');
INSERT INTO `mBody_type` VALUES ('00000000000000000006', '监测体类型5');
INSERT INTO `mBody_type` VALUES ('00000000000000000007', '监测体类型6');
INSERT INTO `mBody_type` VALUES ('00000000000000000008', '监测体类型7');
INSERT INTO `mBody_type` VALUES ('00000000000000000009', '监测体类型8');
INSERT INTO `mBody_type` VALUES ('00000000000000000010', '监测体类型9');

-- ----------------------------
-- Table structure for `mPoint`
-- ----------------------------
DROP TABLE IF EXISTS `mPoint`;
CREATE TABLE `mPoint` (
  `mPoint_id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '监测点编号',
  `mPoint_begin` bigint(20) NOT NULL COMMENT '起始里程',
  `mPoint_end` bigint(20) NOT NULL COMMENT '结束里程',
  `mPoint_type` varchar(30) NOT NULL COMMENT '监测类型',
  `mPoint_is_section` bit(1) NOT NULL COMMENT '是否区段',
  PRIMARY KEY (`mPoint_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of mPoint
-- ----------------------------
INSERT INTO `mPoint` VALUES ('00000000000000000001', '1446', '1572', '监测点类型5', '');
INSERT INTO `mPoint` VALUES ('00000000000000000002', '574', '1034', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000003', '1175', '2435', '监测点类型4', '');
INSERT INTO `mPoint` VALUES ('00000000000000000004', '1016', '2590', '监测点类型6', '');
INSERT INTO `mPoint` VALUES ('00000000000000000005', '998', '1515', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000006', '1543', '2714', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000007', '256', '433', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000008', '143', '239', '监测点类型6', '');
INSERT INTO `mPoint` VALUES ('00000000000000000009', '1333', '2324', '监测点类型6', '');
INSERT INTO `mPoint` VALUES ('00000000000000000010', '1451', '1468', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000011', '325', '815', '监测点类型1', '');
INSERT INTO `mPoint` VALUES ('00000000000000000012', '1956', '3802', '监测点类型7', '');
INSERT INTO `mPoint` VALUES ('00000000000000000013', '502', '1692', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000014', '24', '1824', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000015', '1019', '2492', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000016', '36', '462', '监测点类型1', '');
INSERT INTO `mPoint` VALUES ('00000000000000000017', '590', '1074', '监测点类型1', '');
INSERT INTO `mPoint` VALUES ('00000000000000000018', '1594', '2660', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000019', '806', '1224', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000020', '12', '1099', '监测点类型0', '');
INSERT INTO `mPoint` VALUES ('00000000000000000021', '961', '2471', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000022', '14', '1864', '监测点类型6', '');
INSERT INTO `mPoint` VALUES ('00000000000000000023', '203', '1409', '监测点类型7', '');
INSERT INTO `mPoint` VALUES ('00000000000000000024', '1831', '2910', '监测点类型7', '');
INSERT INTO `mPoint` VALUES ('00000000000000000025', '1269', '2745', '监测点类型1', '');
INSERT INTO `mPoint` VALUES ('00000000000000000026', '256', '970', '监测点类型0', '');
INSERT INTO `mPoint` VALUES ('00000000000000000027', '1390', '2534', '监测点类型5', '');
INSERT INTO `mPoint` VALUES ('00000000000000000028', '1235', '2327', '监测点类型7', '');
INSERT INTO `mPoint` VALUES ('00000000000000000029', '397', '470', '监测点类型1', '');
INSERT INTO `mPoint` VALUES ('00000000000000000030', '1708', '2138', '监测点类型2', '');
INSERT INTO `mPoint` VALUES ('00000000000000000031', '1252', '1527', '监测点类型5', '');
INSERT INTO `mPoint` VALUES ('00000000000000000032', '923', '2181', '监测点类型1', '');
INSERT INTO `mPoint` VALUES ('00000000000000000033', '61', '1693', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000034', '864', '983', '监测点类型4', '');
INSERT INTO `mPoint` VALUES ('00000000000000000035', '849', '1538', '监测点类型0', '');
INSERT INTO `mPoint` VALUES ('00000000000000000036', '525', '1784', '监测点类型5', '');
INSERT INTO `mPoint` VALUES ('00000000000000000037', '1164', '2528', '监测点类型4', '');
INSERT INTO `mPoint` VALUES ('00000000000000000038', '455', '1967', '监测点类型3', '');
INSERT INTO `mPoint` VALUES ('00000000000000000039', '58', '647', '监测点类型6', '');
INSERT INTO `mPoint` VALUES ('00000000000000000040', '277', '350', '监测点类型3', '');

-- ----------------------------
-- Table structure for `mPoint_type`
-- ----------------------------
DROP TABLE IF EXISTS `mPoint_type`;
CREATE TABLE `mPoint_type` (
  `mPoint_type_id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '监测点类型编号',
  `mPoint_type` varchar(255) NOT NULL COMMENT '监测点类型',
  PRIMARY KEY (`mPoint_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COMMENT='监测点类型';

-- ----------------------------
-- Records of mPoint_type
-- ----------------------------
INSERT INTO `mPoint_type` VALUES ('00000000000000000001', '监测点类型0');
INSERT INTO `mPoint_type` VALUES ('00000000000000000002', '监测点类型1');
INSERT INTO `mPoint_type` VALUES ('00000000000000000003', '监测点类型2');
INSERT INTO `mPoint_type` VALUES ('00000000000000000004', '监测点类型3');
INSERT INTO `mPoint_type` VALUES ('00000000000000000005', '监测点类型4');
INSERT INTO `mPoint_type` VALUES ('00000000000000000006', '监测点类型5');
INSERT INTO `mPoint_type` VALUES ('00000000000000000007', '监测点类型6');
INSERT INTO `mPoint_type` VALUES ('00000000000000000008', '监测点类型7');
INSERT INTO `mPoint_type` VALUES ('00000000000000000009', '监测点类型8');
INSERT INTO `mPoint_type` VALUES ('00000000000000000010', '监测点类型9');

-- ----------------------------
-- Table structure for `project`
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `project_id` bigint(20) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT '项目编号',
  `project_name` varchar(50) DEFAULT NULL COMMENT '项目名',
  `project_dept_id` bigint(20) DEFAULT NULL COMMENT '所属单位编号',
  `project_status` varchar(50) DEFAULT NULL COMMENT '项目状态',
  `project_province` varchar(20) DEFAULT NULL COMMENT '省',
  `project_city` varchar(20) DEFAULT NULL COMMENT '市',
  PRIMARY KEY (`project_id`) USING BTREE,
  UNIQUE KEY `project_name` (`project_name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('00000000000000000003', '1608482257013', '22', 'xxx', '河南', '郑州');
INSERT INTO `project` VALUES ('00000000000000000004', '1608482261357', '22', 'xxx', '西藏', '昌都市');
INSERT INTO `project` VALUES ('00000000000000000005', '1608482261408', '22', 'xxx', '海南', '文昌');
INSERT INTO `project` VALUES ('00000000000000000006', '1608482261429', '22', 'xxx', '江苏', '淮安');
INSERT INTO `project` VALUES ('00000000000000000007', '1608482261474', '22', 'xxx', '青海', '果洛');
INSERT INTO `project` VALUES ('00000000000000000008', '1608482261484', '22', 'xxx', '福建', '南平');
INSERT INTO `project` VALUES ('00000000000000000009', '1608482261496', '22', 'xxx', '贵州', '六盘水');
INSERT INTO `project` VALUES ('00000000000000000010', '1608482261516', '22', 'xxx', '甘肃', '庆阳');
INSERT INTO `project` VALUES ('00000000000000000011', '1608482261529', '22', 'xxx', '内蒙古', '海拉尔');
INSERT INTO `project` VALUES ('00000000000000000012', '1608482261561', '22', 'xxx', '新疆', '伊宁');
INSERT INTO `project` VALUES ('00000000000000000013', '1608482261589', '22', 'xxx', '湖南', '吉首');
INSERT INTO `project` VALUES ('00000000000000000014', '1608482261636', '22', 'xxx', '四川', '凉山');
INSERT INTO `project` VALUES ('00000000000000000015', '1608482261653', '22', 'xxx', '福建', '三明');
INSERT INTO `project` VALUES ('00000000000000000016', '1608482261674', '22', 'xxx', '江西', '上饶');
INSERT INTO `project` VALUES ('00000000000000000017', '1608482261689', '22', 'xxx', '江苏', '常州');
INSERT INTO `project` VALUES ('00000000000000000018', '1608482261708', '22', 'xxx', '四川', '成都');
INSERT INTO `project` VALUES ('00000000000000000019', '1608482261725', '22', 'xxx', '黑龙江', '绥化');
INSERT INTO `project` VALUES ('00000000000000000020', '1608482261748', '22', 'xxx', '海南', '三亚');
INSERT INTO `project` VALUES ('00000000000000000021', '1608482261771', '22', 'xxx', '山西', '运城');
INSERT INTO `project` VALUES ('00000000000000000022', '1608482261788', '22', 'xxx', '天津', '天津');
INSERT INTO `project` VALUES ('00000000000000000023', '1608482261814', '22', 'xxx', '山东', '烟台');
INSERT INTO `project` VALUES ('00000000000000000024', '1608482261851', '22', 'xxx', '西藏', '那曲地区');
INSERT INTO `project` VALUES ('00000000000000000025', '1608482261873', '22', 'xxx', '山东', '烟台');
INSERT INTO `project` VALUES ('00000000000000000026', '1608482261879', '22', 'xxx', '江西', '上饶');
INSERT INTO `project` VALUES ('00000000000000000027', '1608482261925', '22', 'xxx', '福建', '漳州');
INSERT INTO `project` VALUES ('00000000000000000028', '1608482261953', '22', 'xxx', '西藏', '那曲地区');
INSERT INTO `project` VALUES ('00000000000000000029', '1608482261976', '22', 'xxx', '吉林', '吉林');
INSERT INTO `project` VALUES ('00000000000000000030', '1608482262007', '22', 'xxx', '安徽', '六安');
INSERT INTO `project` VALUES ('00000000000000000031', '1608482262026', '22', 'xxx', '江苏', '南京');
INSERT INTO `project` VALUES ('00000000000000000032', '1608482262036', '22', 'xxx', '湖北', '荆州');
INSERT INTO `project` VALUES ('00000000000000000033', '1608482262059', '22', 'xxx', '广东', '梅州');
INSERT INTO `project` VALUES ('00000000000000000034', '1608482262095', '22', 'xxx', '重庆', '重庆');
INSERT INTO `project` VALUES ('00000000000000000035', '1608482262128', '22', 'xxx', '上海', '上海');
INSERT INTO `project` VALUES ('00000000000000000036', '1608482262139', '22', 'xxx', '河南', '鹤壁');
INSERT INTO `project` VALUES ('00000000000000000037', '1608482262155', '22', 'xxx', '吉林', '白城');
INSERT INTO `project` VALUES ('00000000000000000038', '1608482262183', '22', 'xxx', '辽宁', '铁岭');
INSERT INTO `project` VALUES ('00000000000000000039', '1608482262214', '22', 'xxx', '西藏', '昌都市');
INSERT INTO `project` VALUES ('00000000000000000040', '1608482262227', '22', 'xxx', '广东', '云浮');
INSERT INTO `project` VALUES ('00000000000000000041', '1608482262239', '22', 'xxx', '云南', '丽江');
INSERT INTO `project` VALUES ('00000000000000000042', '1608482262257', '22', 'xxx', '新疆', '阿勒泰');
INSERT INTO `project` VALUES ('00000000000000000043', '1608482262287', '22', 'xxx', '四川', '凉山');
INSERT INTO `project` VALUES ('00000000000000000044', '1608482262314', '22', 'xxx', '湖南', '常德');
INSERT INTO `project` VALUES ('00000000000000000045', '1608482262335', '22', 'xxx', '西藏', '阿里地区');
INSERT INTO `project` VALUES ('00000000000000000046', '1608482262355', '22', 'xxx', '安徽', '滁州');
INSERT INTO `project` VALUES ('00000000000000000047', '1608482262380', '22', 'xxx', '四川', '乐山');
INSERT INTO `project` VALUES ('00000000000000000048', '1608482262402', '22', 'xxx', '西藏', '林芝市');
INSERT INTO `project` VALUES ('00000000000000000049', '1608482262409', '22', 'xxx', '广西', '阳朔');
INSERT INTO `project` VALUES ('00000000000000000050', '1608482262420', '22', 'xxx', '山东', '东营');
INSERT INTO `project` VALUES ('00000000000000000051', '1608482262445', '22', 'xxx', '青海', '黄南');
INSERT INTO `project` VALUES ('00000000000000000052', '1608482262461', '22', 'xxx', '江西', '宜春');
