package com.tsy.moniter.project;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tsy.moniter.project.entity.*;
import com.tsy.moniter.project.feign.RenrenFastFeignService;
import com.tsy.moniter.project.service.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.UUID;


//测试类要加public才能执行
@SpringBootTest
@RunWith(SpringRunner.class)
public class MoniterProjectApplicationTests {

    @Autowired
    ProjectService projectService;
    @Autowired
    MareaService mareaService;
    @Autowired
    MbodyService mbodyService;
    @Autowired
    MbodyTypeService mbodyTypeService;
    @Autowired
    MpointService mpointService;
    @Autowired
    MpointTypeService mpointTypeService;
    @Autowired
    LocationService locationService;
    @Autowired
    cityMap cityMap;
    @Autowired
    AreaPointService areaPointService;


    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    RedissonClient redissonClient;

    @Test
    public void redisTest(){
        ValueOperations ops = redisTemplate.opsForValue();
        ops.set("hello","world");
    }

    @Test
    public void redisson(){
        System.out.println(redissonClient);
    }

    Random r = new Random();

//    @Test
//    public void addLocation(){
//        for (int i = 0; i < cityMap.province.length; i++) {
//            LocationEntity locationEntity = new LocationEntity();
//            locationEntity.setLocationLevel(1);
//            locationEntity.setLocationName(cityMap.province[i]);
//            locationEntity.setLocationPid(0L);
//            locationService.save(locationEntity);
//        }
//        for (String s : cityMap.model.keySet()) {
//            List<LocationEntity> location_name = locationService.list(new QueryWrapper<LocationEntity>().eq("location_name", s));
//            Long pid = location_name.get(0).getLocationId();
//            Integer plevel = location_name.get(0).getLocationLevel();
//            for (String s1 : cityMap.model.get(s)) {
//                LocationEntity locationEntity = new LocationEntity();
//                locationEntity.setLocationPid(pid);
//                locationEntity.setLocationName(s1);
//                locationEntity.setLocationLevel(plevel+1);
//                locationService.save(locationEntity);
//            }
//        }
//    }
//
//    @Test
//    public void addProject(){
//        List<DeptEntity> deptEntities = deptService.getBaseMapper().selectList(null);
//        String[] deptnames = new String[deptEntities.size()];
//        for (int i = 0; i < deptEntities.size(); i++) {
//            deptnames[i] = deptEntities.get(i).getDeptName();
//        }
//        String[] status= new String[]{"进行中","未开始","以完成","暂停"};
//        for (int i = 0; i < 30; i++) {
//            List<LocationEntity> L1 = locationService.getBaseMapper().selectList(
//                    new QueryWrapper<LocationEntity>().eq("location_pid", 0)
//            );
//            LocationEntity location_first = L1.get(r.nextInt(L1.size()));
//            List<LocationEntity> L2 = locationService.getBaseMapper().selectList(
//                    new QueryWrapper<LocationEntity>().eq("location_pid", location_first.getLocationId())
//            );
//            LocationEntity location_second = L2.get(r.nextInt(L2.size()));
//            ProjectEntity projectEntity = new ProjectEntity();
//            projectEntity.setProjectName("项目"+System.currentTimeMillis()%10000);
//            projectEntity.setProjectStatus(status[r.nextInt(status.length)]);
//            projectEntity.setProjectLocation(location_first.getLocationName()+location_second.getLocationName());
//           // projectEntity.setProjectDeptName(deptnames[r.nextInt(deptnames.length)]);
//            projectService.save(projectEntity);
//        }
//    }


    @Test
    public void addMarea(){

        List<ProjectEntity> projectEntityList = projectService.list();
        Long[] projectIds = new Long[projectEntityList.size()];
        for (int i = 0; i < projectIds.length; i++) {
            projectIds[i] = projectEntityList.get(i).getProjectId();
        }
        for (int i = 0; i < 100; i++) {
            MareaEntity mareaEntity = new MareaEntity();
            mareaEntity.setMareaAlertLevel(r.nextInt(4)+1);
            mareaEntity.setMareaBegin((long) r.nextInt(2000));
            mareaEntity.setMareaEnd((mareaEntity.getMareaBegin()+ r.nextInt(10000)));
            mareaEntity.setProjectId(projectEntityList.get(r.nextInt(projectEntityList.size())).getProjectId());
            mareaService.save(mareaEntity);
        }
    }

    @Test
    public void addMbody(){
        List<ProjectEntity> projectEntityList = projectService.list();
        Long[] projectIds = new Long[projectEntityList.size()];
        for (int i = 0; i < projectIds.length; i++) {
            projectIds[i] = projectEntityList.get(i).getProjectId();
        }
        for (int i = 0; i < 40; i++) {
            MbodyEntity mbodyEntity = new MbodyEntity();
            mbodyEntity.setMbodyBegin((long) r.nextInt(2000));
            mbodyEntity.setMbodyEnd(mbodyEntity.getMbodyBegin()+ r.nextInt(2000));
            mbodyEntity.setMbodyIsSection(r.nextInt() & 1);
            mbodyEntity.setMbodyName("监测体" + UUID.randomUUID());
            mbodyEntity.setMbodyStatus("状态"+ (i%3 +1));
            mbodyEntity.setMbodyType("监测体类型" + r.nextInt(5));
            mbodyEntity.setProjectId(projectIds[r.nextInt(projectIds.length)]);
            mbodyService.save(mbodyEntity);
        }
    }
    @Test
    public void addMpoint(){
        List<MbodyEntity> mbodyEntities = mbodyService.getBaseMapper().selectList(null);
        for (int i = 0; i < 400; i++) {
            MbodyEntity mbodyEntity = mbodyEntities.get(r.nextInt(mbodyEntities.size()));
            MpointEntity mpointEntity = new MpointEntity();
            mpointEntity.setMpointBegin((long) r.nextInt(200));
            mpointEntity.setMpointEnd(mpointEntity.getMpointBegin() + r.nextInt(300));
            mpointEntity.setMpointIsSection(r.nextInt() & 1);
            mpointEntity.setMpointType(r.nextLong()%5 + 1);
            mpointEntity.setMbodyId(mbodyEntity.getMbodyId());
            mpointEntity.setProjectId(mbodyEntity.getProjectId());
            mpointService.save(mpointEntity);
        }
    }
    @Test
    public void addMareaMpointRelation(){
        List<MareaEntity> mareaEntities = mareaService.getBaseMapper().selectList(null);
        for (int i = 0; i < 5; i++) {
            int temp = i;
            MareaEntity mareaEntity = mareaEntities.get(temp);
            mareaEntities.remove(temp);
            Long projectId = mareaEntity.getProjectId();
            List<MpointEntity> mpointEntityList = mpointService.list(new QueryWrapper<MpointEntity>().eq("project_id", projectId));
            for (int j = 0; j <mpointEntityList.size()/10 ; j++) {
                AreaPointEntity areaPointEntity = new AreaPointEntity();
                areaPointEntity.setMareaId(mareaEntity.getMareaId());
                areaPointEntity.setMpointId(mpointEntityList.get(j).getMpointId());
                areaPointEntity.setProjectId(projectId);
                areaPointService.save(areaPointEntity);
            }
        }
    }
//    @Test
//    public void addMpointType(){
//        for (int i = 0; i < 10; i++) {
//           MpointTypeEntity mpointTypeEntity = new MpointTypeEntity();
//           mpointTypeEntity.setMpointType("监测点类型" + i);
//           mpointTypeService.save(mpointTypeEntity);
//        }
//    }

//    @Test
//    public void addMbodyType(){
//        for (int i = 0; i < 10; i++) {
//            MbodyTypeEntity mbodyTypeEntity = new MbodyTypeEntity();
//            mbodyTypeEntity.setMbodyType("监测体类型" + i);
//            mbodyTypeService.save(mbodyTypeEntity);
//        }
//    }
}
