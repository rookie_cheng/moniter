package com.tsy.moniter.project.service.impl;

import com.tsy.moniter.project.entity.ProjectEntity;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.MpointTypeDao;
import com.tsy.moniter.project.entity.MpointTypeEntity;
import com.tsy.moniter.project.service.MpointTypeService;
import org.springframework.util.StringUtils;


@Service("mpointTypeService")
public class MpointTypeServiceImpl extends ServiceImpl<MpointTypeDao, MpointTypeEntity> implements MpointTypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<MpointTypeEntity> wrapper = new QueryWrapper<>();
        String key = (String)params.get("key");
        if (!StringUtils.isEmpty(key)){
            System.out.println(key);
            wrapper.like("mpoint_type",key)
                    .or().eq("mpoint_type_id",key);
        }
        IPage<MpointTypeEntity> page = this.page(new Query<MpointTypeEntity>().getPage(params),
                wrapper
        );
        return new PageUtils(page);
    }

}