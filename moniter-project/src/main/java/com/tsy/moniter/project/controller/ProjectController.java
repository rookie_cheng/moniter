package com.tsy.moniter.project.controller;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.feign.RenrenFastFeignService;
import com.tsy.moniter.project.vo.ProjectNamesAndIdsVo;
import com.tsy.moniter.project.vo.SysDeptEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.ProjectEntity;
import com.tsy.moniter.project.service.ProjectService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;


/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 15:49:57
 */
@Api(tags = "项目相关接口")
@RestController
@RequestMapping("project/project")
@Slf4j
public class ProjectController {
    @Autowired
    private ProjectService projectService;
    @Autowired
    private HttpServletRequest request;

    private static final String CacheName = "project";

    /**
     * 以列表形态获取项目信息
     */
    @ApiOperation(value = "以列表形态获取项目名及Id，返回 List<ProjectNamesAndIdsVo>",httpMethod = "POST",response = ProjectNamesAndIdsVo.class)
    @RequestMapping("/projectNamesAndIds")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName")
    public R getprojectNamesAndIds(){
        List<ProjectEntity> projectEntities = projectService.getprojectList( new HashMap<String, Object>());
        List<ProjectNamesAndIdsVo> projectNamesAndIdsVos = new LinkedList<>();
        for (ProjectEntity projectEntity : projectEntities) {
            ProjectNamesAndIdsVo projectNamesAndIdsVo = new ProjectNamesAndIdsVo();
            projectNamesAndIdsVo.setProjectId(projectEntity.getProjectId());
            projectNamesAndIdsVo.setProjectName(projectEntity.getProjectName());
            projectNamesAndIdsVos.add(projectNamesAndIdsVo);
        }
        return R.ok().put("data", projectNamesAndIdsVos);
    }


    /**
     * 以列表形态获取项目信息
     */
    @ApiOperation(value = "以列表形态获取项目所有信息，返回List<ProjectEntity>",response = ProjectEntity.class,httpMethod = "POST")
    @RequestMapping("/projectList")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName")
    public R projectList(){
        List<ProjectEntity> projectEntities = projectService.getprojectList( new HashMap<String, Object>());
        return R.ok().put("data", projectEntities);
    }


    /**
     * 列表
     * 获得所有项目信息，以页的形式返回出去
     */
    @ApiOperation(value = "以page形态查询项目所有信息",response = PageUtils.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    //这个key是把param里的values里除了时间t之外的值联合起来
   // @Cacheable(value = {CacheName},sync = true,key = "#params.values().toString().substring(#params.get('t').length()+1)")
    @RequiresPermissions("project:project:list")
    public R list(@RequestParam HashMap<String, Object> params){

        //根据token获得userId
//        String token = request.getHeader("token");
//        SysUserEntity user = renrenFastFeignService.getUserByToken(token);
//        log.info("userId: "+ user.getUserId());

        PageUtils page = projectService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据项目id获取项目信息",httpMethod = "POST",response = ProjectEntity.class)
    @ApiImplicitParam(name = "projectId",value = "项目编号",dataType = "Integer")
    @RequestMapping("/info/{projectId}")
    @RequiresPermissions("project:project:info")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #projectId")
    public R info(@PathVariable("projectId") Long projectId){
		ProjectEntity project = projectService.getById(projectId);

        return R.ok().put("project", project);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存项目",httpMethod = "POST")
    @ApiImplicitParam(name = "project",value = "项目实体",dataTypeClass = ProjectEntity.class)
    @RequestMapping("/save")
    @SysLog("保存项目")
    @RequiresPermissions("project:project:save")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除project下所有缓存
    public R save(@RequestBody ProjectEntity project){
        boolean save = projectService.save(project);
        if (save)
        return R.ok();
        //TODO 判断失败的类型
        return R.error();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改项目",httpMethod = "POST")
    @ApiImplicitParam(name = "project",value = "项目实体",dataTypeClass = ProjectEntity.class)
    @RequestMapping("/update")
    @SysLog("更新项目")
    @RequiresPermissions("project:project:update")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除project下所有缓存
    public R update(@RequestBody ProjectEntity project){


		projectService.updateById(project);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据项目id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "projectIds",value = "项目编号数组",dataType = "Long[]")

    @RequestMapping("/delete")
    @SysLog("删除项目")
    @RequiresPermissions("project:project:delete")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除project下所有缓存
    public R delete(@RequestBody Long[] projectIds){
        //TODO 具体的删除逻辑目前不明

		projectService.removeByIds(Arrays.asList(projectIds));
        return R.ok();
    }

    @ApiIgnore
    @RequestMapping("/updateDept")
    @CacheEvict(cacheNames =CacheName,allEntries = true) //删除project下所有缓存
    public R updateDept(@RequestBody SysDeptEntity sysDeptEntity){
        log.debug("updateDept被调用了");
        System.out.println("sysDeptEntity: "+sysDeptEntity.toString());
        Long deptId = sysDeptEntity.getDeptId();
        ProjectEntity projectEntity = new ProjectEntity();
        projectEntity.setDeptName(sysDeptEntity.getName());
        projectService.update(projectEntity,new QueryWrapper<ProjectEntity>().eq("dept_id",deptId));
        return R.ok();
    }

}
