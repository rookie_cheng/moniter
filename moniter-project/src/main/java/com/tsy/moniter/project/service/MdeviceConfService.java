package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.moniter.project.entity.MdeviceConfEntity;

import java.util.Map;

/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2021-01-11 21:13:09
 */
public interface MdeviceConfService extends IService<MdeviceConfEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

