package com.tsy.moniter.project.service.impl;

import com.tsy.moniter.project.annotation.DataFilter;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.MdeviceConfDao;
import com.tsy.moniter.project.entity.MdeviceConfEntity;
import com.tsy.moniter.project.service.MdeviceConfService;


@Service("mdeviceConfService")
public class MdeviceConfServiceImpl extends ServiceImpl<MdeviceConfDao, MdeviceConfEntity> implements MdeviceConfService {

    @Override
    @DataFilter
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MdeviceConfEntity> page = this.page(
                new Query<MdeviceConfEntity>().getPage(params),
                new QueryWrapper<MdeviceConfEntity>()
        );

        return new PageUtils(page);
    }

}