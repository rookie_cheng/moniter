package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 监测点类型
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@ApiModel("监测点类型实体")
@Data
@TableName("mpoint_type")
public class MpointTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 监测点类型编号
	 */
	@ApiModelProperty("监测点类型编号")
	@TableId
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mpointTypeId;
	/**
	 * 监测点类型
	 */
	@ApiModelProperty("监测点类型描述")
	private String mpointType;

}
