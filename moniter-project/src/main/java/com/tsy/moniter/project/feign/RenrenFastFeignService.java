package com.tsy.moniter.project.feign;


import com.tsy.common.utils.R;
import com.tsy.moniter.project.vo.SysLogEntity;
import com.tsy.moniter.project.vo.SysMenuEntity;
import com.tsy.moniter.project.vo.SysUserEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;


/**
 * 这是一个声明式的远程调用
 */
@FeignClient("renren-fast")
public interface RenrenFastFeignService {

    @PostMapping("renren-fast/sys/userToken/getUserByToken")
    SysUserEntity getUserByToken(@RequestBody String token);
    @PostMapping("renren-fast/sys/log/save")
    R saveLog(@RequestBody SysLogEntity log);
    @PostMapping("renren-fast/sys/dept/getSubDeptIdList/{userId}")
     List<Long> getSubDeptIdList(@PathVariable("userId") long userId);
    @PostMapping("renren-fast/sys/dept/info/{deptId}")
     R deptInfo(@PathVariable("deptId") Long deptId);
    @PostMapping("renren-fast/sys/menu/list")
     List<SysMenuEntity> menuList();
    @GetMapping("renren-fast/sys/menu/muneListByUserId")
     List<String> queryAllPerms(Long userId);
}
