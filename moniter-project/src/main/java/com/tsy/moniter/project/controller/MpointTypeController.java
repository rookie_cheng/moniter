package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.Map;

import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.entity.MbodyTypeEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.MpointTypeEntity;
import com.tsy.moniter.project.service.MpointTypeService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;



/**
 * 监测点类型
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@Api(tags = "监测点类型相关接口")
@RestController
@RequestMapping("project/mpointtype")
public class MpointTypeController {
    @Autowired
    private MpointTypeService mpointTypeService;

    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询监测点类型所有信息",response = MpointTypeEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    @RequiresPermissions("project:mpointtype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = mpointTypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据监测点类型id获取项目信息",httpMethod = "POST",response = MpointTypeEntity.class)
    @ApiImplicitParam(name = "mpointTypeId",value = "监测点类型编号",dataType = "Integer")
    @RequestMapping("/info/{mpointTypeId}")
    @RequiresPermissions("project:mpointtype:info")
    public R info(@PathVariable("mpointTypeId") Long mpointTypeId){
		MpointTypeEntity mpointType = mpointTypeService.getById(mpointTypeId);

        return R.ok().put("mpointType", mpointType);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存",httpMethod = "POST")
    @ApiImplicitParam(name = "mpointType",value = "类型实体",dataTypeClass = MpointTypeEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测体类型")
    @RequiresPermissions("project:mpointtype:save")
    public R save(@RequestBody MpointTypeEntity mpointType){
		mpointTypeService.save(mpointType);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改",httpMethod = "POST")
    @ApiImplicitParam(name = "mpointType",value = "类型实体",dataTypeClass = MpointTypeEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测体类型")
    @RequiresPermissions("project:mpointtype:update")
    public R update(@RequestBody MpointTypeEntity mpointType){
		mpointTypeService.updateById(mpointType);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据类型id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "mpointTypeIds",value = "编号数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测体类型")
    @RequiresPermissions("project:mpointtype:delete")
    public R delete(@RequestBody Long[] mpointTypeIds){
		mpointTypeService.removeByIds(Arrays.asList(mpointTypeIds));

        return R.ok();
    }

}
