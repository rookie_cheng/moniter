package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.tsy.moniter.project.annotation.SysLog;

import com.tsy.moniter.project.service.MbodyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.MareaEntity;
import com.tsy.moniter.project.service.MareaService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;



/**
 * 监测域表
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:00:09
 */
@Api(tags = "监测域相关接口")
@RestController
@RequestMapping("project/marea")
@Slf4j
public class MareaController {
    @Autowired
    private MareaService mareaService;

    @Autowired
    private MbodyService mbodyService;

    private static final  String CacheName = "marea";
    /**
     * 根据项目id获取监测域Id列表
     */
    @ApiOperation(value = "根据项目id获取监测域Id列表，返回list",httpMethod = "POST")
    @ApiImplicitParam(name = "projectId",value = "项目编号",dataType = "Integer")
    @RequestMapping("/mareaIds/{projectId}")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #projectId")
   // @RequiresPermissions("project:dept:deptList")
    public R getmareaList(@PathVariable("projectId") Long projectId){
        List<MareaEntity> mareaEntities = mareaService.getBaseMapper().selectList(
                new QueryWrapper<MareaEntity>().eq("project_id", projectId)
        );
        List<String> mareaIds = new LinkedList<>();
        for (MareaEntity mareaEntity : mareaEntities) {
            mareaIds.add(""+mareaEntity.getMareaId());//以字符串的方式发送给前端，不然会丧失精度
        }
        return R.ok().put("data", mareaIds);
    }



    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询监测域所有信息",response = MareaEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )

    @RequestMapping("/list")
    @RequiresPermissions("project:marea:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = mareaService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据监测域id获取项目信息",httpMethod = "POST",response = MareaEntity.class)
    @ApiImplicitParam(name = "mareaId",value = "监测域编号",dataType = "Integer")
    @RequestMapping("/info/{mareaId}")
    @RequiresPermissions("project:marea:info")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #mareaId")
    public R info(@PathVariable("mareaId") Long mareaId){
		MareaEntity marea = mareaService.getById(mareaId);

        return R.ok().put("marea", marea);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存监测域",httpMethod = "POST")
    @ApiImplicitParam(name = "marea",value = "监测域实体",dataTypeClass = MareaEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测域")
    @RequiresPermissions("project:marea:save")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R save(@RequestBody MareaEntity marea){

		mareaService.save(marea);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改监测域",httpMethod = "POST")
    @ApiImplicitParam(name = "marea",value = "监测域实体",dataTypeClass = MareaEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测域")
    @RequiresPermissions("project:marea:update")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R update(@RequestBody MareaEntity marea){



		mareaService.updateById(marea);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据监测域id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "mareaIds",value = "监测域编号数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测域")
    @RequiresPermissions("project:marea:delete")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R delete(@RequestBody Long[] mareaIds){
		mareaService.removeByIds(Arrays.asList(mareaIds));

        return R.ok();
    }

}
