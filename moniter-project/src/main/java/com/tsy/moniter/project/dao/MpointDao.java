package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.MpointEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@Mapper
public interface MpointDao extends BaseMapper<MpointEntity> {
	
}
