package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 监测域表
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:00:09
 */
@ApiModel("监测域实体")
@Data
@TableName("marea")
public class MareaEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 监测域编号
	 */
	@ApiModelProperty("监测域编号")
	@TableId(type = IdType.ASSIGN_ID)//mybatisPlus3.3.0开始这IdType.ASSIGN_ID代表使用雪花算法来生成主键id
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mareaId;
	/**
	 * 告警级别
	 */
	@ApiModelProperty("告警级别")
	private Integer mareaAlertLevel;
	/**
	 * 起始里程
	 */
	@ApiModelProperty("起始里程")
	private Long mareaBegin;
	/**
	 * 结束里程
	 */
	@ApiModelProperty("结束里程")
	private Long mareaEnd;

	/**
	 * 所属项目的编号
	 */
	@ApiModelProperty("所属项目的编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long projectId;

	@ApiModelProperty("所属项目的名称，实际表中没有，但是为了页面显示会在返回给页面的时候插入")
	@TableField(exist=false)
	private String projectName;
}
