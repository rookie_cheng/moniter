package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;
import com.tsy.moniter.project.entity.MbodyEntity;
import com.tsy.moniter.project.entity.ProjectEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 监测体表
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
public interface MbodyService extends IService<MbodyEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<MbodyEntity> getMbodyList(HashMap<String, Object> params);
    R removeByIdsDetail(List<Long> asList);
    Map<Long,String> MbodyIdToName();
}

