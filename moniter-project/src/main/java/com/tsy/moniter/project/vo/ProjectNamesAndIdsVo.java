package com.tsy.moniter.project.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

@Data
public class ProjectNamesAndIdsVo {
    @JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
    private Long projectId;
    /**
     * 项目名
     */
    private String projectName;
}
