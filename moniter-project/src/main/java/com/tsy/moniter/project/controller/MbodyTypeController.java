package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.Map;

import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.entity.ProjectEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.MbodyTypeEntity;
import com.tsy.moniter.project.service.MbodyTypeService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;



/**
 * 监测体类型表
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:46
 */
@Api(tags = "监测体类型相关接口")
@RestController
@RequestMapping("project/mbodytype")
public class MbodyTypeController {
    @Autowired
    private MbodyTypeService mbodyTypeService;

    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询监测体类型所有信息",response = MbodyTypeEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    @RequiresPermissions("project:mbodytype:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = mbodyTypeService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据监测体类型id获取项目信息",httpMethod = "POST",response = MbodyTypeEntity.class)
    @ApiImplicitParam(name = "mbodyTypeId",value = "监测体类型编号",dataType = "Integer")
    @RequestMapping("/info/{mbodyTypeId}")
    @RequiresPermissions("project:mbodytype:info")
    public R info(@PathVariable("mbodyTypeId") Long mbodyTypeId){
		MbodyTypeEntity mbodyType = mbodyTypeService.getById(mbodyTypeId);

        return R.ok().put("mbodyType", mbodyType);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存监测体类型",httpMethod = "POST")
    @ApiImplicitParam(name = "mbodyType",value = "监测体类型实体",dataTypeClass = MbodyTypeEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测体类型")
    @RequiresPermissions("project:mbodytype:save")
    public R save(@RequestBody MbodyTypeEntity mbodyType){
		mbodyTypeService.save(mbodyType);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改监测体类型",httpMethod = "POST")
    @ApiImplicitParam(name = "mbodyType",value = "监测体类型实体",dataTypeClass = MbodyTypeEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测体类型")
    @RequiresPermissions("project:mbodytype:update")
    public R update(@RequestBody MbodyTypeEntity mbodyType){
		mbodyTypeService.updateById(mbodyType);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据类型id删除类型",httpMethod = "POST")
    @ApiImplicitParam(name = "mbodyTypeIds",value = "编号数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测体类型")
    @RequiresPermissions("project:mbodytype:delete")
    public R delete(@RequestBody Long[] mbodyTypeIds){
		mbodyTypeService.removeByIds(Arrays.asList(mbodyTypeIds));

        return R.ok();
    }

}
