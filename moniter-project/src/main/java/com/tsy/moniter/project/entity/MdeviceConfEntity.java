package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2021-01-11 21:13:09
 */
@ApiModel("设备配置实体")
@Data
@TableName("mdevice_conf")
public class MdeviceConfEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@ApiModelProperty("主键，无现实意义")
	@TableId(type = IdType.ASSIGN_ID)//mybatisPlus3.3.0开始这IdType.ASSIGN_ID代表使用雪花算法来生成主键id
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mdeviceConfId;
	/**
	 * 设备编号
	 */
	@ApiModelProperty("设备编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long deviceId;
	/**
	 * 监测点编号
	 */
	@ApiModelProperty("监测点编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mpointId;

}
