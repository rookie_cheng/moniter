package com.tsy.moniter.project.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.LocationDao;
import com.tsy.moniter.project.entity.LocationEntity;
import com.tsy.moniter.project.service.LocationService;


@Service("locationService")
public class LocationServiceImpl extends ServiceImpl<LocationDao, LocationEntity> implements LocationService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<LocationEntity> page = this.page(
                new Query<LocationEntity>().getPage(params),
                new QueryWrapper<LocationEntity>()
        );

        return new PageUtils(page);
    }

    @Override
    public List<LocationEntity> listWithTree() {
        List<LocationEntity> entities = baseMapper.selectList(null);
        List<LocationEntity> level1Menus = entities.stream().filter(categoryEntity ->
                categoryEntity.getLocationPid() == 0
        ).map((menu)->{
            menu.setChildren(getChildrens(menu,entities));
            return menu;
        }).collect(Collectors.toList());
        return level1Menus;
    }
    //递归查找所有菜单的子菜单
    private List<LocationEntity> getChildrens(LocationEntity root,List<LocationEntity> all){

        List<LocationEntity> children = all.stream().filter(categoryEntity -> {
            return categoryEntity.getLocationPid().equals(root.getLocationId());
        }).map(categoryEntity -> {
            //1、找到子菜单
            categoryEntity.setChildren(getChildrens(categoryEntity,all));
            return categoryEntity;
        }).collect(Collectors.toList());

        return children;
    }
}