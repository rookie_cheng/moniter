package com.tsy.moniter.project.exception;


import com.tsy.common.utils.R;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 集中处理所有异常
 */
@Slf4j
@RestControllerAdvice(basePackages = "com.tsy.moniter.project.controller")
public class projectExceptionControllerAdvice {
    @ExceptionHandler(value = DuplicateKeyException.class)
    public R handleException(Throwable throwable){
        log.error("错误：", throwable);
        return R.error(100,"重复");
    }
}
