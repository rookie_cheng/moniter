package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.entity.MareaEntity;
import com.tsy.moniter.project.entity.MbodyEntity;
import com.tsy.moniter.project.service.MbodyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.MpointEntity;
import com.tsy.moniter.project.service.MpointService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;



/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@Api(tags = "监测点相关接口")
@RestController
@RequestMapping("project/mpoint")
public class MpointController {
    @Autowired
    private MpointService mpointService;

    @Autowired
    private MbodyService mbodyService;

    private static final String CacheName = "mpoint";
    /**
     * 根据项目id获取监测点列表
     */
    @ApiOperation(value = "根据项目id获取监测点列表，返回list",httpMethod = "POST",response = MpointEntity.class)
    @ApiImplicitParam(name = "projectId",value = "项目编号",dataType = "Long")
    @RequestMapping("/mpointIds/{projectId}")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #projectId")
    public R mpointList(@PathVariable("projectId") Long projectId){

        List<MpointEntity> mpointEntities = mpointService.getBaseMapper().selectList(
                new QueryWrapper<MpointEntity>().eq("project_id", projectId)
        );
        List<String> mpointIds = new LinkedList<>();
        for (MpointEntity mpointEntity : mpointEntities) {
            mpointIds.add(""+mpointEntity.getMpointId());//以字符串的方式发送给前端，不然会丧失精度
        }
        return R.ok().put("data", mpointIds);
    }



    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询监测点所有信息",response = MpointEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )

    @RequestMapping("/list")
    @RequiresPermissions("project:mpoint:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = mpointService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 列表,根据projectid来查询
     */
    @ApiOperation(value = "以page形态根据根据projectid来查询查询监测域所有信息",response = MareaEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @ApiImplicitParam(name = "projectId",value = "项目编号",dataType = "Integer")
    @RequestMapping("/list/{projectId}")
    public R list(@RequestParam Map<String, Object> params,
                  @PathVariable("projectId") Long projectId){

        PageUtils page = mpointService.queryPage(params,projectId);

        return R.ok().put("page", page);
    }



    /**
     * 信息
     */
    @ApiOperation(value = "根据监测点id获取项目信息",httpMethod = "POST",response = MpointEntity.class)
    @ApiImplicitParam(name = "mpointId",value = "监测点编号",dataType = "Integer")
    @RequestMapping("/info/{mpointId}")
    @RequiresPermissions("project:mpoint:info")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #mpointId")
    public R info(@PathVariable("mpointId") Long mpointId){
		MpointEntity mpoint = mpointService.getById(mpointId);

        return R.ok().put("mpoint", mpoint);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存",httpMethod = "POST")
    @ApiImplicitParam(name = "mpoint",value = "实体",dataTypeClass = MpointEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测点")
    @RequiresPermissions("project:mpoint:save")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R save(@RequestBody MpointEntity mpoint){
        //根据监测体编号得出所属项目编号
        Long mbodyId = mpoint.getMbodyId();
        if (mbodyId != null){
            Long projectId = mbodyService.getById(mbodyId).getProjectId();
            if (mpoint.getProjectId()!=null){
                if (!projectId.equals(mpoint.getProjectId()))
                    return R.error("监测体与项目冲突");
            }else {
                mpoint.setProjectId(projectId);
            }
        }
        mpointService.save(mpoint);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改",httpMethod = "POST")
    @ApiImplicitParam(name = "mpoint",value = "实体",dataTypeClass = MpointEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测点")
    @RequiresPermissions("project:mpoint:update")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R update(@RequestBody MpointEntity mpoint){
        //根据监测体编号得出所属项目编号
        Long mbodyId = mpoint.getMbodyId();
        if (mbodyId != null){
            Long projectId = mbodyService.getById(mbodyId).getProjectId();
            if (mpoint.getProjectId()!=null){
                if (!projectId.equals(mpoint.getProjectId()))
                    return R.error("监测体与项目冲突");
            }else {
                mpoint.setProjectId(projectId);
            }
        }

		mpointService.updateById(mpoint);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据监测点id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "mpointIds",value = "监测域编号数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测点")
    @RequiresPermissions("project:mpoint:delete")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R delete(@RequestBody Long[] mpointIds){
		mpointService.removeByIds(Arrays.asList(mpointIds));

        return R.ok();
    }

}
