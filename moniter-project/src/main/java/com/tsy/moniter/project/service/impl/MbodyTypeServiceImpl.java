package com.tsy.moniter.project.service.impl;

import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.MbodyTypeDao;
import com.tsy.moniter.project.entity.MbodyTypeEntity;
import com.tsy.moniter.project.service.MbodyTypeService;


@Service("mbodyTypeService")
public class MbodyTypeServiceImpl extends ServiceImpl<MbodyTypeDao, MbodyTypeEntity> implements MbodyTypeService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MbodyTypeEntity> page = this.page(
                new Query<MbodyTypeEntity>().getPage(params),
                new QueryWrapper<MbodyTypeEntity>()
        );

        return new PageUtils(page);
    }

}