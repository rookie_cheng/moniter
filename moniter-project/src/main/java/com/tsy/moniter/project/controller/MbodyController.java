package com.tsy.moniter.project.controller;

import java.util.*;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.entity.MareaEntity;
import com.tsy.moniter.project.entity.ProjectEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.MbodyEntity;
import com.tsy.moniter.project.service.MbodyService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;



/**
 * 监测体表
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@Api(tags = "监测体相关接口")
@RestController
@RequestMapping("project/mbody")
public class MbodyController {
    @Autowired
    private MbodyService mbodyService;
    private static final  String CacheName = "mbody";
    /**
     * 根据项目id获取监测体列表
     */
    @ApiOperation(value = "根据项目id获取监测体Id列表，返回list",httpMethod = "POST",response = MbodyEntity.class)
    @ApiImplicitParam(name = "projectId",value = "项目编号",dataType = "Integer")
    @RequestMapping("/mbodyListByProjectId/{projectId}")
    //@RequiresPermissions("project:dept:deptList")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #projectId")
    public R getmbodyListByProjectId(@PathVariable("projectId") Long projectId){
        List<MbodyEntity> mbodyEntities = mbodyService.getBaseMapper().selectList(
                new QueryWrapper<MbodyEntity>().eq("project_id", projectId)
        );
        return R.ok().put("data", mbodyEntities);
    }




    /**
     * 以列表形态获取监测体信息
     */
    @RequestMapping("/mbodyList")
    @ApiOperation(value = "以列表形态获取项目所有信息",response = ProjectEntity.class,httpMethod = "POST")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName")
    public R mbodyList(){
        List<MbodyEntity> mbodyEntities = mbodyService.getMbodyList(new HashMap<String, Object>());
        return R.ok().put("data", mbodyEntities);
    }
    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询监测体所有信息",response = MbodyEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    @RequiresPermissions("project:mbody:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = mbodyService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据监测体id获取项目信息",httpMethod = "POST",response = MbodyEntity.class)
    @ApiImplicitParam(name = "mbodyId",value = "监测体编号",dataType = "Integer")
    @RequestMapping("/info/{mbodyId}")
    @RequiresPermissions("project:mbody:info")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #mbodyId")
    public R info(@PathVariable("mbodyId") Long mbodyId){
		MbodyEntity mbody = mbodyService.getById(mbodyId);

        return R.ok().put("mbody", mbody);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存监测体",httpMethod = "POST")
    @ApiImplicitParam(name = "mbody",value = "监测体实体",dataTypeClass = MbodyEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测体")
    @RequiresPermissions("project:mbody:save")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R save(@RequestBody MbodyEntity mbody){
		mbodyService.save(mbody);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改监测体",httpMethod = "POST")
    @ApiImplicitParam(name = "mbody",value = "监测体实体",dataTypeClass = MbodyEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测体")
    @RequiresPermissions("project:mbody:update")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R update(@RequestBody MbodyEntity mbody){
		mbodyService.updateById(mbody);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据监测体id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "mbodyIds",value = "项目编号数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测体")
    @RequiresPermissions("project:mbody:delete")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R delete(@RequestBody Long[] mbodyIds){

        return mbodyService.removeByIdsDetail(Arrays.asList(mbodyIds));
    }

}
