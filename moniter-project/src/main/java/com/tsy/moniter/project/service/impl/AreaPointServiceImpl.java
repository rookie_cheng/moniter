package com.tsy.moniter.project.service.impl;

import com.tsy.common.utils.Constant;
import com.tsy.moniter.project.annotation.DataFilter;
import com.tsy.moniter.project.entity.MbodyEntity;
import com.tsy.moniter.project.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.AreaPointDao;
import com.tsy.moniter.project.entity.AreaPointEntity;
import com.tsy.moniter.project.service.AreaPointService;
import org.springframework.util.StringUtils;


@Service("areaPointService")
public class AreaPointServiceImpl extends ServiceImpl<AreaPointDao, AreaPointEntity> implements AreaPointService {

    @Autowired
    ProjectService projectService;

    @Override
    @DataFilter
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<AreaPointEntity> queryWrapper = new QueryWrapper<>();
        //1、获取projectid
        String projectid = (String) params.get("projectId");
        if(!StringUtils.isEmpty(projectid)){
            queryWrapper.eq("project_id",projectid);
        }
        //2.获取key
        String key = (String) params.get("key");
        if(!StringUtils.isEmpty(key)){
          //TODO 填写查询规则
        }

        IPage<AreaPointEntity> page = this.page(
                new Query<AreaPointEntity>().getPage(params),
                queryWrapper.apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );

        Map<Long, String> projectIdToName = projectService.projectIdToName();
        List<AreaPointEntity> records = page.getRecords();
        for (AreaPointEntity record : records) {
            record.setProjectName(projectIdToName.get(record.getProjectId()));
        }

        return new PageUtils(page);
    }

}