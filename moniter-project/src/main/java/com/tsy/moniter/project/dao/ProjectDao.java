package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.ProjectEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 15:49:57
 */
@Mapper
public interface ProjectDao extends BaseMapper<ProjectEntity> {
	
}
