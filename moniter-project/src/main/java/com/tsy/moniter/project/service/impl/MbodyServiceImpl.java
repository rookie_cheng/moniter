package com.tsy.moniter.project.service.impl;

import com.tsy.common.utils.Constant;
import com.tsy.common.utils.R;
import com.tsy.moniter.project.annotation.DataFilter;
import com.tsy.moniter.project.entity.MareaEntity;
import com.tsy.moniter.project.entity.MpointEntity;
import com.tsy.moniter.project.entity.ProjectEntity;
import com.tsy.moniter.project.service.MareaService;
import com.tsy.moniter.project.service.MpointService;
import com.tsy.moniter.project.service.ProjectService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.MbodyDao;
import com.tsy.moniter.project.entity.MbodyEntity;
import com.tsy.moniter.project.service.MbodyService;


@Service("mbodyService")
public class MbodyServiceImpl extends ServiceImpl<MbodyDao, MbodyEntity> implements MbodyService {

    @Autowired
    MareaService mareaService;
    @Autowired
    MpointService mpointService;

    @Autowired
    ProjectService projectService;

    @Override
    @DataFilter
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MbodyEntity> page = this.page(
                new Query<MbodyEntity>().getPage(params),
                new QueryWrapper<MbodyEntity>()
                        .apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        Map<Long, String> projectIdToName = projectService.projectIdToName();
        List<MbodyEntity> records = page.getRecords();
        for (MbodyEntity record : records) {
            record.setProjectName(projectIdToName.get(record.getProjectId()));
        }
        return new PageUtils(page);
    }

    @Override
    @DataFilter
    public List<MbodyEntity> getMbodyList(HashMap<String, Object> params) {
        return baseMapper.selectList(new QueryWrapper<MbodyEntity>().apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER)));
    }


    @Override
    public R removeByIdsDetail(List<Long> asList) {
        for (Long aLong : asList) {
            if (mareaService.list(new QueryWrapper<MareaEntity>().eq("mbody_id",aLong)).size()!=0)
                return R.error("第"+aLong+"号监测体存在未删除的监测域");
        }
        for (Long aLong : asList) {
            if (mpointService.list(new QueryWrapper<MpointEntity>().eq("mbody_id",aLong)).size()!=0)
                return R.error("第"+aLong+"号监测体存在未删除的监测点");
        }
        baseMapper.deleteBatchIds(asList);
        return R.ok();
    }
    public Map<Long,String> MbodyIdToName(){
        List<MbodyEntity> mbodyEntities = this.getMbodyList(new HashMap<>());
        Map<Long,String> mbodyIdToName = new HashMap<>();
        for (MbodyEntity mbodyEntity : mbodyEntities) {
            mbodyIdToName.put(mbodyEntity.getMbodyId(),mbodyEntity.getMbodyName());
        }
        return mbodyIdToName;
    }

}