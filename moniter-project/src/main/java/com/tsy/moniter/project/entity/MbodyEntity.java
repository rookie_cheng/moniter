package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 监测体表
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@ApiModel("监测体实体")
@Data
@TableName("mbody")
public class MbodyEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 检测体编号
	 */
	@ApiModelProperty("监测体编号")
	@TableId(type = IdType.ASSIGN_ID)//mybatisPlus3.3.0开始这IdType.ASSIGN_ID代表使用雪花算法来生成主键id
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mbodyId;
	/**
	 * 监测体名称
	 */
	@ApiModelProperty("监测体名称")
	private String mbodyName;
	/**
	 * 监测体类型
	 */
	@ApiModelProperty("监测体类型")
	private String mbodyType;
	/**
	 * 监测体状态
	 */
	@ApiModelProperty("监测体状态")
	private String mbodyStatus;
	/**
	 * 是否区段
	 */
	@ApiModelProperty("是否区段，0表示否，1表示是")
	private Integer mbodyIsSection;
	/**
	 * 起始里程
	 */
	@ApiModelProperty("起始里程")
	private Long mbodyBegin;
	/**
	 * 结束里程
	 */
	@ApiModelProperty("结束里程")
	private Long mbodyEnd;
	/**
	 * 所属项目id
	 */
	@ApiModelProperty("所属项目id")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long projectId;

	@ApiModelProperty("所属项目的名称，实际表中没有，但是为了页面显示会在返回给页面的时候插入")
	@TableField(exist=false)
	private String projectName;
//	@JsonInclude(JsonInclude.Include.NON_EMPTY)
//	@TableField(exist=false)
//	private List<DeptEntity> children;

}
