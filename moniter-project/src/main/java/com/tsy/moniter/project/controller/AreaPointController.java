package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.Map;

import com.tsy.moniter.project.annotation.DataFilter;
import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.entity.ProjectEntity;
import com.tsy.moniter.project.service.MareaService;
import com.tsy.moniter.project.service.MpointService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.AreaPointEntity;
import com.tsy.moniter.project.service.AreaPointService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:07:11
 */
@Api(tags = "监测域与监测点关系相关接口")
@RestController
@RequestMapping("project/areapoint")
public class AreaPointController {
    @Autowired
    private AreaPointService areaPointService;

    @Autowired
    private MareaService mareaService;

    @Autowired
    private MpointService mpointService;

    private static final  String CacheName = "areapoint";
    /**
     * 列表
     */
    @ApiOperation(value = "以页面形态查询监测点与监测域关系",httpMethod = "POST",response = PageUtils.class)
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    @RequiresPermissions("project:areapoint:list")
    public R list(@RequestParam Map<String, Object> params){

        PageUtils page = areaPointService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiIgnore
    @RequestMapping("/info/{apId}")
    @RequiresPermissions("project:areapoint:info")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #apId")
    public R info(@PathVariable("apId") Long apId){
		AreaPointEntity areaPoint = areaPointService.getById(apId);

        return R.ok().put("areaPoint", areaPoint);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存监测域与监测点的关系",httpMethod = "POST")
    @ApiImplicitParam(name = "areaPoint",value = "监测域与监测点的关系实体",dataTypeClass = AreaPointEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测域与监测点的关系")
    @RequiresPermissions("project:areapoint:save")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R save(@RequestBody AreaPointEntity areaPoint){
        Long projectId1 = mareaService.getById(areaPoint.getMareaId()).getProjectId();
        Long projectId2 = mpointService.getById(areaPoint.getMpointId()).getProjectId();
        if (!projectId1.equals(projectId2))
            return R.error("监测域和监测点不属于同一个项目");
        areaPoint.setProjectId(mareaService.getById(areaPoint.getMareaId()).getProjectId());

		areaPointService.save(areaPoint);
        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改监测域与监测点的关系",httpMethod = "POST")
    @ApiImplicitParam(name = "areaPoint",value = "监测域与监测点的关系实体",dataTypeClass = AreaPointEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测域与监测点的关系")
    @RequiresPermissions("project:areapoint:update")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R update(@RequestBody AreaPointEntity areaPoint){
        Long projectId1 = mareaService.getById(areaPoint.getMareaId()).getProjectId();
        Long projectId2 = mpointService.getById(areaPoint.getMpointId()).getProjectId();
        if (!projectId1.equals(projectId2))
            return R.error("监测域和监测点不属于同一个项目");
        areaPoint.setProjectId(mareaService.getById(areaPoint.getMareaId()).getProjectId());

		areaPointService.updateById(areaPoint);
        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据主键id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "apIds",value = "项目编号数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测域与监测点的关系")
    @RequiresPermissions("project:areapoint:delete")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R delete(@RequestBody Long[] apIds){
		areaPointService.removeByIds(Arrays.asList(apIds));

        return R.ok();
    }

}
