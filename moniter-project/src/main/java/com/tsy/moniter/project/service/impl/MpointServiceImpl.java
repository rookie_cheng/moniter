package com.tsy.moniter.project.service.impl;

import com.tsy.common.utils.Constant;
import com.tsy.moniter.project.annotation.DataFilter;
import com.tsy.moniter.project.entity.MbodyEntity;
import com.tsy.moniter.project.entity.ProjectEntity;
import com.tsy.moniter.project.service.MbodyService;
import com.tsy.moniter.project.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.MpointDao;
import com.tsy.moniter.project.entity.MpointEntity;
import com.tsy.moniter.project.service.MpointService;
import org.springframework.util.StringUtils;


@Service("mpointService")
public class MpointServiceImpl extends ServiceImpl<MpointDao, MpointEntity> implements MpointService {

    @Autowired
    ProjectService projectService;
    @Autowired
    MbodyService mbodyService;


    @Override
    @DataFilter
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<MpointEntity> wrapper = new QueryWrapper<>();

        String key = (String)params.get("key");
        if (!StringUtils.isEmpty(key)){
            System.out.println(key);
            wrapper.eq("mpoint_id",key);
        }
        IPage<MpointEntity> page = this.page(new Query<MpointEntity>().getPage(params),
                wrapper.apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        Map<Long, String> projectIdToName = projectService.projectIdToName();
        Map<Long, String> mbodyIdToName = mbodyService.MbodyIdToName();
        List<MpointEntity> records = page.getRecords();
        for (MpointEntity record : records) {
            record.setProjectName(projectIdToName.get(record.getProjectId()));
            record.setMbodyName(mbodyIdToName.get(record.getMbodyId()));
        }
        return new PageUtils(page);
    }

    @Override
    public PageUtils queryPage(Map<String, Object> params, Long projectId) {
        QueryWrapper<MpointEntity> wrapper = new QueryWrapper<>();
        wrapper.eq("project_id",projectId);
        IPage<MpointEntity> iPage = this.page(new Query<MpointEntity>().getPage(params),
                wrapper.apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        String projectName = projectService.getById(projectId).getProjectName();
        Map<Long, String> mbodyIdToName = mbodyService.MbodyIdToName();
        List<MpointEntity> records = iPage.getRecords();
        for (MpointEntity record : records) {
            record.setProjectName(projectName);
            record.setMbodyName(mbodyIdToName.get(record.getMbodyId()));
        }
        return new PageUtils(iPage);
    }

}