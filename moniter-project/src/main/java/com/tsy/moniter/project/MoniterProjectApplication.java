package com.tsy.moniter.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@EnableCaching      //开启缓存功能
@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.tsy.moniter.project.dao")
@EnableFeignClients(basePackages = "com.tsy.moniter.project.feign")
public class MoniterProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MoniterProjectApplication.class, args);
	}

}
