package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 14:17:40
 */
@ApiModel("地址实体")
@Data
@TableName("location")
public class LocationEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	@ApiModelProperty("主键，无现实意义")
	@TableId
	private Long locationId;
	/**
	 * 
	 */
	@ApiModelProperty("地址等级，如江苏省为1级，苏州市为2级")
	private Integer locationLevel;
	/**
	 * 
	 */
	@ApiModelProperty("地址名称")
	private String locationName;
	/**
	 * 
	 */
	@ApiModelProperty("该地址的父id，如苏州市的父id是江苏省的id")
	private Long locationPid;
	@ApiModelProperty("如江苏省的children为｛苏州，南京等等｝")
	@JsonInclude(JsonInclude.Include.NON_EMPTY)
	@TableField(exist=false)
	private List<LocationEntity> children;
}
