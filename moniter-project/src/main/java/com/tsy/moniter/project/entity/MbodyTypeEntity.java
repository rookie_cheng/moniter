package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 监测体类型表
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:46
 */
@ApiModel("监测体类型实体")
@Data
@TableName("mbody_type")
public class MbodyTypeEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 监测体类型编号
	 */
	@ApiModelProperty("监测体类型编号")
	@TableId
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mbodyTypeId;
	/**
	 * 监测体类型名称
	 */
	@ApiModelProperty("监测体类型名称")
	private String mbodyType;

}
