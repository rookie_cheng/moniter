package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:07:11
 */
@ApiModel("监测域与监测点关系实体")
@Data
@TableName("area_point")
public class AreaPointEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 监测域编号
	 */
	@ApiModelProperty("监测域编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mareaId;
	/**
	 * 监测点编号
	 */
	@ApiModelProperty("监测点编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mpointId;
	/**
	 * 
	 */
	@ApiModelProperty("主键，无现实意义")
	@TableId
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long apId;

	/**
	 * 项目编号
	 */
	@ApiModelProperty("项目编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long projectId;

	@ApiModelProperty("所属项目的名称，实际表中没有，但是为了页面显示会在返回给页面的时候插入")
	@TableField(exist=false)
	private String projectName;
}
