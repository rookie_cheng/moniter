package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.LocationEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 14:17:40
 */
@Mapper
public interface LocationDao extends BaseMapper<LocationEntity> {
	
}
