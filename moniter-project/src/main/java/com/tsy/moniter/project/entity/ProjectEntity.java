package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 15:49:57
 */
@ApiModel("项目实体")
@Data
@TableName("project")
public class ProjectEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 项目编号
	 */
	@ApiModelProperty("项目编号")
	@TableId(type = IdType.ASSIGN_ID)//mybatisPlus3.3.0开始这IdType.ASSIGN_ID代表使用雪花算法来生成主键id
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long projectId;
	/**
	 * 项目名
	 */
	@ApiModelProperty("项目名")
	private String projectName;
	/**
	 * 所属单位Id
	 */
	@ApiModelProperty("所属单位Id")
	private Long deptId;

	@ApiModelProperty("所属单位名")
	private String deptName;
	/**
	 * 项目状态
	 */
	@ApiModelProperty("项目状态")
	private String projectStatus;
	/**
	 * 项目地址
	 */
	@ApiModelProperty("项目地址")
	private String projectLocation;

//	@JsonInclude(JsonInclude.Include.NON_EMPTY)
//	@TableField(exist=false)
//	private List<DeptEntity> children;

}
