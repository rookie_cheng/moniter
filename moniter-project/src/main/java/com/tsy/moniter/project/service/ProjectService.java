package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.moniter.project.entity.ProjectEntity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 15:49:57
 */
public interface ProjectService extends IService<ProjectEntity> {

    PageUtils queryPage(Map<String, Object> params);

    List<ProjectEntity> getprojectList(HashMap<String, Object> params);
     Map<Long,String> projectIdToName();
}

