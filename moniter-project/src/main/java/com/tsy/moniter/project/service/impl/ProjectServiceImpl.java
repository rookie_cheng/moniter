package com.tsy.moniter.project.service.impl;


import com.tsy.common.utils.Constant;
import com.tsy.moniter.project.annotation.DataFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.ProjectDao;
import com.tsy.moniter.project.entity.ProjectEntity;
import com.tsy.moniter.project.service.ProjectService;
import org.springframework.util.StringUtils;

@Service("projectService")
public class ProjectServiceImpl extends ServiceImpl<ProjectDao, ProjectEntity> implements ProjectService {


    @Override
    @DataFilter
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<ProjectEntity> wrapper = new QueryWrapper<>();

        //根据状态查询
        String status = (String)params.get("status");
        if (!StringUtils.isEmpty(status)){
            wrapper.eq("project_status",status);
        }
        //根据查询关键字查询
        String key = (String)params.get("key");
        if (!StringUtils.isEmpty(key)){
            System.out.println(key);
            wrapper.like("project_location",key)
                    .or().eq("project_id",key)
                    .or().like("project_name",key)
                    .or().like("project_status",key);
        }
        IPage<ProjectEntity> page = this.page(new Query<ProjectEntity>().getPage(params),
                wrapper.apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        return new PageUtils(page);
    }

    /**
     * 以列表形态获取项目信息
     */
    //每一个缓存的数据都指定放到哪个名字的缓存【缓存分区（按照业务类型分）】
    @Cacheable(value = {"project"},sync = true,key = "#root.methodName") //代表当前方法的结果需要缓存，如果缓存中有则方法不调用，如果没有就调用方法并将结果放入缓存
    @Override
    @DataFilter
    public List<ProjectEntity> getprojectList(HashMap<String, Object> params) {
        return baseMapper.selectList(new QueryWrapper<ProjectEntity>().apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER)));
    }

    public Map<Long,String> projectIdToName(){
        List<ProjectEntity> projectEntities = this.getprojectList(new HashMap<>());
        Map<Long,String> projectIdToName = new HashMap<>();
        for (ProjectEntity projectEntity : projectEntities) {
            projectIdToName.put(projectEntity.getProjectId(),projectEntity.getProjectName());
        }
        return projectIdToName;
    }
}