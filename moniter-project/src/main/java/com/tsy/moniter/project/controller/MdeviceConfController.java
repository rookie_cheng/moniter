package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.Map;

import com.tsy.moniter.project.annotation.SysLog;
import com.tsy.moniter.project.entity.ProjectEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.MdeviceConfEntity;
import com.tsy.moniter.project.service.MdeviceConfService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;



/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2021-01-11 21:13:09
 */
@Api(tags = "设备配置相关接口")
@RestController
@RequestMapping("project/mdeviceconf")
public class MdeviceConfController {
    @Autowired
    private MdeviceConfService mdeviceConfService;

    private static final  String CacheName = "mdeviceConf";
    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询设备配置所有信息",response = MdeviceConfEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    @RequiresPermissions("project:mdeviceconf:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = mdeviceConfService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据id获取信息",httpMethod = "POST",response = MdeviceConfEntity.class)
    @ApiImplicitParam(name = "mdeviceConfId",value = "id",dataType = "Integer")
    @RequestMapping("/info/{mdeviceConfId}")
    @RequiresPermissions("project:mdeviceconf:info")
    @Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #mdeviceConfId")
    public R info(@PathVariable("mdeviceConfId") Long mdeviceConfId){
		MdeviceConfEntity mdeviceConf = mdeviceConfService.getById(mdeviceConfId);

        return R.ok().put("mdeviceConf", mdeviceConf);
    }

    /**
     * 保存
     */
    @ApiOperation(value = "保存",httpMethod = "POST")
    @ApiImplicitParam(name = "mdeviceConf",value = "实体",dataTypeClass = MdeviceConfEntity.class)
    @RequestMapping("/save")
    @SysLog("保存监测逻辑与设备配置")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    @RequiresPermissions("project:mdeviceconf:save")
    public R save(@RequestBody MdeviceConfEntity mdeviceConf){
		mdeviceConfService.save(mdeviceConf);

        return R.ok();
    }

    /**
     * 修改
     */
    @ApiOperation(value = "修改",httpMethod = "POST")
    @ApiImplicitParam(name = "mdeviceConf",value = "实体",dataTypeClass = MdeviceConfEntity.class)
    @RequestMapping("/update")
    @SysLog("更新监测逻辑与设备配置")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    @RequiresPermissions("project:mdeviceconf:update")
    public R update(@RequestBody MdeviceConfEntity mdeviceConf){
		mdeviceConfService.updateById(mdeviceConf);

        return R.ok();
    }

    /**
     * 删除
     */
    @ApiOperation(value = "根据id删除项目",httpMethod = "POST")
    @ApiImplicitParam(name = "mdeviceConfIds",value = "id数组",dataType = "Long[]")
    @RequestMapping("/delete")
    @SysLog("删除监测逻辑与设备配置")
    @RequiresPermissions("project:mdeviceconf:delete")
    @CacheEvict(cacheNames = CacheName,allEntries = true) //删除本CacheName下所有缓存
    public R delete(@RequestBody Long[] mdeviceConfIds){
		mdeviceConfService.removeByIds(Arrays.asList(mdeviceConfIds));

        return R.ok();
    }

}
