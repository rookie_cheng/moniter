package com.tsy.moniter.project.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 00:50:46
 */
@ApiModel("监测点实体")
@Data
@TableName("mpoint")
public class MpointEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 监测点编号
	 */
	@ApiModelProperty("监测点编号")
	@TableId(type = IdType.ASSIGN_ID)//mybatisPlus3.3.0开始这IdType.ASSIGN_ID代表使用雪花算法来生成主键id
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mpointId;
	/**
	 * 起始里程
	 */
	@ApiModelProperty("起始里程")
	private Long mpointBegin;
	/**
	 * 结束里程
	 */
	@ApiModelProperty("结束里程")
	private Long mpointEnd;
	/**
	 * 监测点类型编号
	 */
	@ApiModelProperty("监测点类型编号")
	private Long mpointType;
	/**
	 * 是否区段
	 */
	@ApiModelProperty("是否区段")
	private Integer mpointIsSection;
	/**
	 * 所属监测体的编号
	 */
	@ApiModelProperty("所属监测体的编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long mbodyId;

	@ApiModelProperty("所属监测体的名称。实际数据库表中没有")
	@TableField(exist=false)
	private String mbodyName;
	/**
	 * 所属项目的编号
	 */
	@ApiModelProperty("所属项目的编号")
	@JsonSerialize(using= ToStringSerializer.class)//传给前端JS时将Long型转换为字符串，保持精度
	private Long projectId;
	@ApiModelProperty("所属项目的名称。实际数据库表中没有")
	@TableField(exist=false)
	private String projectName;

}
