package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.moniter.project.entity.MpointTypeEntity;

import java.util.Map;

/**
 * 监测点类型
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
public interface MpointTypeService extends IService<MpointTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

