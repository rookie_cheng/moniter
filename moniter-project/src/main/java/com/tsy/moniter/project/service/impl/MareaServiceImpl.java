package com.tsy.moniter.project.service.impl;

import com.tsy.common.utils.Constant;
import com.tsy.moniter.project.annotation.DataFilter;
import com.tsy.moniter.project.entity.ProjectEntity;
import com.tsy.moniter.project.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.Query;

import com.tsy.moniter.project.dao.MareaDao;
import com.tsy.moniter.project.entity.MareaEntity;
import com.tsy.moniter.project.service.MareaService;


@Service("mareaService")
public class MareaServiceImpl extends ServiceImpl<MareaDao, MareaEntity> implements MareaService {
    @Autowired
    ProjectService projectService;

    @Override
    @DataFilter
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<MareaEntity> page = this.page(
                new Query<MareaEntity>().getPage(params),
                new QueryWrapper<MareaEntity>().apply(params.get(Constant.SQL_FILTER) != null, (String)params.get(Constant.SQL_FILTER))
        );
        Map<Long, String> projectIdToName = projectService.projectIdToName();


        List<MareaEntity> records = page.getRecords();
        for (MareaEntity record : records) {
            record.setProjectName(projectIdToName.get(record.getProjectId()));
        }
        return new PageUtils(page);
    }

}