package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.MdeviceConfEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2021-01-11 21:13:09
 */
@Mapper
public interface MdeviceConfDao extends BaseMapper<MdeviceConfEntity> {
	
}
