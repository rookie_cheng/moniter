package com.tsy.moniter.project.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.tsy.moniter.project.entity.ProjectEntity;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tsy.moniter.project.entity.LocationEntity;
import com.tsy.moniter.project.service.LocationService;
import com.tsy.common.utils.PageUtils;
import com.tsy.common.utils.R;
import springfox.documentation.annotations.ApiIgnore;


/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-23 14:17:40
 */
@Api(tags = "地址相关接口")
@RestController
@RequestMapping("project/location")
public class LocationController {
    @Autowired
    private LocationService locationService;

    /**
     * 查出所有分类以及子分类，以树形结构组装起来
     */
    @ApiOperation(value = "出所有分类以及子分类，以树形结构组装起来,返回一个列表",httpMethod = "POST",response = LocationEntity.class )
    @RequestMapping("/list/tree")
    public R list(){
        List<LocationEntity> entities = locationService.listWithTree();
        return R.ok().put("data",entities);
    }


    /**
     * 列表
     */
    @ApiOperation(value = "以page形态查询地址所有信息",response = LocationEntity.class,httpMethod = "POST")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "第几页",dataType = "Integer"),
            @ApiImplicitParam(name = "limit",value = "一页多少项",dataType = "Integer"),
            @ApiImplicitParam(name = "key",value = "搜索的关键词",dataType = "String")
    }
    )
    @RequestMapping("/list")
    //@RequiresPermissions("project:location:list")
    public R list(@RequestParam Map<String, Object> params){
        PageUtils page = locationService.queryPage(params);

        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @ApiOperation(value = "根据项目id获取项目信息",httpMethod = "POST",response = LocationEntity.class)
    @ApiImplicitParam(name = "locationId",value = "地址编号",dataType = "Integer")
    @RequestMapping("/info/{locationId}")
    //@RequiresPermissions("project:location:info")
    public R info(@PathVariable("locationId") Long locationId){
		LocationEntity location = locationService.getById(locationId);

        return R.ok().put("location", location);
    }

//    /**
//     * 保存
//     */
//    @RequestMapping("/save")
//    //@RequiresPermissions("project:location:save")
//    public R save(@RequestBody LocationEntity location){
//		locationService.save(location);
//
//        return R.ok();
//    }
//
//    /**
//     * 修改
//     */
//    @RequestMapping("/update")
//    //@RequiresPermissions("project:location:update")
//    public R update(@RequestBody LocationEntity location){
//		locationService.updateById(location);
//
//        return R.ok();
//    }
//
//    /**
//     * 删除
//     */
//    @RequestMapping("/delete")
//    //@RequiresPermissions("project:location:delete")
//    public R delete(@RequestBody Long[] locationIds){
//		locationService.removeByIds(Arrays.asList(locationIds));
//
//        return R.ok();
//    }

}
