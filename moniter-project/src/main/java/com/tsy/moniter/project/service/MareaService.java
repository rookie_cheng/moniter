package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.moniter.project.entity.MareaEntity;

import java.util.Map;

/**
 * 监测域表
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:00:09
 */
public interface MareaService extends IService<MareaEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

