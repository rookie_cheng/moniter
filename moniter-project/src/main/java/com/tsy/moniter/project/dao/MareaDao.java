package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.MareaEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 监测域表
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:00:09
 */
@Mapper
public interface MareaDao extends BaseMapper<MareaEntity> {
	
}
