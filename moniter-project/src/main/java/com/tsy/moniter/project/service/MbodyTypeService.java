package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.moniter.project.entity.MbodyTypeEntity;

import java.util.Map;

/**
 * 监测体类型表
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:46
 */
public interface MbodyTypeService extends IService<MbodyTypeEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

