package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.MpointTypeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 监测点类型
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-20 23:38:47
 */
@Mapper
public interface MpointTypeDao extends BaseMapper<MpointTypeEntity> {
	
}
