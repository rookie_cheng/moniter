package com.tsy.moniter.project.dao;

import com.tsy.moniter.project.entity.AreaPointEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 
 * 
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:07:11
 */
@Mapper
public interface AreaPointDao extends BaseMapper<AreaPointEntity> {
	
}
