package com.tsy.moniter.project.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.tsy.common.utils.PageUtils;
import com.tsy.moniter.project.entity.AreaPointEntity;

import java.util.Map;

/**
 * 
 *
 * @author zhangcheng
 * @email 1157440840@qq.com
 * @date 2020-12-28 02:07:11
 */
public interface AreaPointService extends IService<AreaPointEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

