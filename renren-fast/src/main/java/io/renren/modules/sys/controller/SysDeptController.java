/**
 * Copyright (c) 2016-2019 人人开源 All rights reserved.
 *
 * https://www.renren.io
 *
 * 版权所有，侵权必究！
 */

package io.renren.modules.sys.controller;

import io.renren.common.annotation.DataFilter;
import io.renren.common.utils.Constant;
import io.renren.common.utils.R;
import io.renren.modules.sys.entity.SysDeptEntity;
import io.renren.modules.sys.entity.SysMenuEntity;
import io.renren.modules.sys.feign.ProjectFeign;
import io.renren.modules.sys.service.SysDeptService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;


/**
 * 部门管理
 *
 * @author Mark sunlightcs@gmail.com
 */
@RestController
@RequestMapping("/sys/dept")
public class SysDeptController extends AbstractController {
	@Autowired
	private SysDeptService sysDeptService;
	@Autowired
	private ProjectFeign projectFeign;
	private static final String CacheName = "dept";
	/**
	 * 列表
	 */
	@RequestMapping("/list")

	@RequiresPermissions("sys:dept:list")
	@Cacheable(value = {CacheName},sync = true,key = "#root.methodName")
	public List<SysDeptEntity> list(){

		List<SysDeptEntity> deptList = sysDeptService.queryList(new HashMap<String, Object>());

		return deptList;
	}

	/**
	 * 选择部门(添加、修改菜单)
	 */
	@RequestMapping("/select")
	@RequiresPermissions("sys:dept:select")
	@Cacheable(value = {CacheName},sync = true,key = "#root.methodName")
	public R select(){
		List<SysDeptEntity> deptList = sysDeptService.queryList(new HashMap<String, Object>());

		//添加一级部门
		if(getUserId() == Constant.SUPER_ADMIN){
			SysDeptEntity root = new SysDeptEntity();
			root.setDeptId(0L);
			root.setName("一级部门");
			root.setParentId(-1L);
			root.setOpen(true);
			deptList.add(root);
		}

		return R.ok().put("deptList", deptList);
	}

	/**
	 * 上级部门Id(管理员则为0)
	 */
	@RequestMapping("/info")
	@RequiresPermissions("sys:dept:list")
	@Cacheable(value = {CacheName},sync = true,key = "#root.methodName")
	public R info(){
		long deptId = 0;
		if(getUserId() != Constant.SUPER_ADMIN){
			List<SysDeptEntity> deptList = sysDeptService.queryList(new HashMap<String, Object>());
			Long parentId = null;
			for(SysDeptEntity sysDeptEntity : deptList){
				if(parentId == null){
					parentId = sysDeptEntity.getParentId();
					continue;
				}

				if(parentId > sysDeptEntity.getParentId().longValue()){
					parentId = sysDeptEntity.getParentId();
				}
			}
			deptId = parentId;
		}

		return R.ok().put("deptId", deptId);
	}
	
	/**
	 * 信息
	 */
	@RequestMapping("/info/{deptId}")
	@RequiresPermissions("sys:dept:info")
	@Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #deptId")
	public R deptInfo(@PathVariable("deptId") Long deptId){
		SysDeptEntity dept = sysDeptService.getById(deptId);
		
		return R.ok().put("dept", dept);
	}
	
	/**
	 * 保存
	 */
	@RequestMapping("/save")
	@RequiresPermissions("sys:dept:save")
	@CacheEvict(cacheNames = CacheName,allEntries = true) //删除project下所有缓存
	public R save(@RequestBody SysDeptEntity dept){
		sysDeptService.save(dept);
		//更新project里的deptName
		projectFeign.updateDept(dept);
		return R.ok();
	}
	
	/**
	 * 修改
	 */
	@RequestMapping("/update")
	@RequiresPermissions("sys:dept:update")
	@CacheEvict(cacheNames = CacheName,allEntries = true) //删除project下所有缓存
	public R update(@RequestBody SysDeptEntity dept){
		sysDeptService.updateById(dept);
		//更新project里的deptName
		projectFeign.updateDept(dept);
		return R.ok();
	}
	
	/**
	 * 删除
	 */
	@PostMapping("/delete/{deptId}")
	@RequiresPermissions("sys:dept:delete")
	@CacheEvict(cacheNames = CacheName,allEntries = true) //删除project下所有缓存
	public R delete(@PathVariable("deptId") long deptId){
		//判断是否有子部门
		List<Long> deptList = sysDeptService.queryDetpIdList(deptId);
		if(deptList.size() > 0){
			return R.error("请先删除子部门");
		}

		sysDeptService.removeById(deptId);
		
		return R.ok();
	}
	@PostMapping("/getSubDeptIdList/{userId}")
	@Cacheable(value = {CacheName},sync = true,key = "#root.methodName+'-' + #userId")
	public List<Long> getSubDeptIdList(@PathVariable("userId") long userId){
		return sysDeptService.getSubDeptIdList(userId);
	}
	
}
