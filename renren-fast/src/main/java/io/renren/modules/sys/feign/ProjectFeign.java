package io.renren.modules.sys.feign;

import io.renren.common.utils.R;
import io.renren.modules.sys.entity.SysDeptEntity;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


/**
 * 这是一个声明式的远程调用
 */
@FeignClient("moniter-project")
public interface ProjectFeign {
    @PostMapping("project/project/updateDept")
    R updateDept(@RequestBody SysDeptEntity sysDeptEntity);
}
