package io.renren.modules.sys.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.renren.common.utils.R;
import io.renren.modules.sys.entity.SysUserEntity;
import io.renren.modules.sys.entity.SysUserTokenEntity;
import io.renren.modules.sys.service.SysUserService;
import io.renren.modules.sys.service.SysUserTokenService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;


@RestController
@RequestMapping("/sys/userToken")
@Slf4j
public class SysUserTokenController {
    @Autowired
    private SysUserTokenService userTokenService;
    @Autowired
    private SysUserService userService;
    @PostMapping("/getUserByToken")
    public SysUserEntity getUserIdByToken(@RequestBody String token){

        Long userId = userTokenService.getBaseMapper().selectOne(new QueryWrapper<SysUserTokenEntity>().eq("token", token)).getUserId();
        SysUserEntity sysUserEntity = userService.getBaseMapper().selectById(userId);
        return sysUserEntity;
    }
}
